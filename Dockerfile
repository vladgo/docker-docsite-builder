FROM docs/docker.github.io:docs-builder

USER root

COPY *.cer /usr/local/share/ca-certificates/

RUN set -x \
  && update-ca-certificates
